/**
 * 
 */
console.log("inside manager js file");

window.onload = function () {
    getAllUsers();
    getAllReimbs();
    getCurrentUser();

    let filterButton = document.getElementById("filterButton");
    filterButton.addEventListener('click', doFiltering);

    let logoutButton = document.getElementById("logout");
    logoutButton.addEventListener('click', function () {
        //calling server to invalidate current session



        document.location.href = 'http://localhost:9001/ProjectOne';
    });

    let modifyForm = document.getElementById("modifyForm");
    modifyForm.addEventListener('submit', modifyReimbursement);

}


function modifyReimbursement(eve) {
    eve.preventDefault();
    const { primaryKey, status } = eve.target;

    const myJSON = {
        reimbId: primaryKey.value,
        statusId: status.value
    }

    if (!isNaN(primaryKey.value)) {
        //see if a reimbursement with that primary key actually exists
        let bool = false;

        for (let i = 0; i < allReimbs.length; i++) {
            console.log(bool);
            if (allReimbs[i].reimbId == primaryKey.value && allReimbs[i].statusId == 1) {
                bool = true;
            }
        }

        if (bool == true) {
            fetch('http://localhost:9001/ProjectOne/api/ajax/modifyReimbursement',

                {
                    method: 'post',
                    'headers': {
                        'Content-Type': 'application/json'
                    },
                    'body': JSON.stringify(myJSON)

                }
            )
            alert("Submission was a success! Refresh the page to see the changes.");

            primaryKey.value = "";
            status.value = "";
        } else {
            alert("You can't modify that reimbursement.");
        }



    } else {
        alert("Please enter a number for the primary key.");
    }

    location.reload();




}


let allReimbs = [];
async function getAllReimbs() {

    const responsePayload = await fetch('http://localhost:9001/ProjectOne/api/ajax/allReimbursements');

    let myJSON = await responsePayload.json();

    console.log(myJSON);

    allReimbs = myJSON;

    ourDOMManipulation(allReimbs);
}


async function getCurrentUser() {

    const responsePayload = await fetch('http://localhost:9001/ProjectOne/api/ajax/currentUser');

    const convertedResponse = await responsePayload.json();

    console.log(convertedResponse);

    domManipulateUser(convertedResponse);

}



let userArray = [];

async function getAllUsers() {

    const responsePayload = await fetch('http://localhost:9001/ProjectOne/api/ajax/getAllUsers');

    userArray = await responsePayload.json();

    console.log(userArray);
}

//function for filtering reimbursements based on form submission
let newReimbArray = [];


function filterReimbs(type) {
    switch (type) {

        case "pending":
            newReimbArray = allReimbs.filter(value => {
                if (value.statusId == 1) {
                    return value;
                }
            });
            return newReimbArray;

        case "approved":
            newReimbArray = allReimbs.filter(value => {
                if (value.statusId == 2) {
                    return value;
                }
            });
            return newReimbArray;

        case "denied":
            newReimbArray = allReimbs.filter(value => {
                if (value.statusId == 3) {
                    return value;
                }
            });
            return newReimbArray;
        case "all":
            return allReimbs;


    }
}


//functionality for filtering reimbursements by status


function doFiltering(eve) {
    //eve.preventDefault();

    let radios = document.getElementsByName('filtertype');

    let filterType;

    for (var i = 0, length = radios.length; i < length; i++) {
        if (radios[i].checked) {

            filterType = radios[i].value;
            console.log(filterType);

            // only one radio can be logically checked, don't check the rest
            break;
        }
    }

    let filtered = filterReimbs(filterType);

    //getting rid of table rows and then adding more
    let table = document.getElementById("reimbTable");
    for (let i = table.rows.length - 1; i > 0; i--) {
        table.deleteRow(i);
    }

    ourDOMManipulation(filtered);
}

function ourDOMManipulation(ourJSON) {
    for (let i = 0; i < ourJSON.length; i++) {

        //all creations
        let newTR = document.createElement("tr");
        let newTH = document.createElement("th");

        let newTD1 = document.createElement("td");
        let newTD2 = document.createElement("td");
        let newTD3 = document.createElement("td");
        let newTD4 = document.createElement("td");
        let newTD5 = document.createElement("td");
        let newTD6 = document.createElement("td");
        let newTD7 = document.createElement("td");
        let newTD8 = document.createElement("td");
        let newTD9 = document.createElement("td");

        //population creations
        newTH.setAttribute("scope", "row");
        let myText1 = document.createTextNode(ourJSON[i].reimbId);
        let myText2 = document.createTextNode("$" + ourJSON[i].amount);

        //switch statments for type and status
        let myText3;
        switch (ourJSON[i].typeId) {
            case 1:
                myText3 = document.createTextNode("Lodging");
                break;
            case 2:
                myText3 = document.createTextNode("Travel");
                break;
            case 3:
                myText3 = document.createTextNode("Food");
                break;
            case 4:
                myText3 = document.createTextNode("Other");
                break;
        }

        let myText4;
        switch (ourJSON[i].statusId) {
            case 1:
                myText4 = document.createTextNode("Pending");
                break;
            case 2:
                myText4 = document.createTextNode("Approved");
                break;
            case 3:
                myText4 = document.createTextNode("Denied");
                break;
        }


        //let myText3 = document.createTextNode(ourJSON[i].typeId);
        //let myText4 = document.createTextNode(ourJSON[i].statusId);
        let myText5 = document.createTextNode(ourJSON[i].submitted ? new Date(ourJSON[i].submitted).toLocaleString() : "N/A");
        let myText6 = document.createTextNode(ourJSON[i].resolved ? new Date(ourJSON[i].resolved).toLocaleString() : "N/A");
        let myText7 = document.createTextNode(ourJSON[i].description);

        //loops to determine which user is which 
        let userFullName = "";
        let resolverFullName = null;

        for (let j = 0; j < userArray.length; j++) {
            //console.log(userArray[j].userId);
            if (userArray[j].userId == ourJSON[i].authorId) {
                userFullName = userArray[j].firstName + " " + userArray[j].lastName;
            }
        }

        for (let j = 0; j < userArray.length; j++) {
            if (userArray[j].userId == ourJSON[i].resolverId) {
                resolverFullName = userArray[j].firstName + " " + userArray[j].lastName;
            }
        }

        let myText8 = document.createTextNode(userFullName);
        let myText9 = document.createTextNode(resolverFullName);


        //all appendings
        newTH.appendChild(myText1);
        newTD1.appendChild(myText2);
        newTD2.appendChild(myText3);
        newTD3.appendChild(myText4);
        newTD4.appendChild(myText5);
        newTD5.appendChild(myText6);
        newTD6.appendChild(myText7);
        newTD7.appendChild(myText8);
        newTD8.appendChild(myText9);


        newTR.appendChild(newTH);
        newTR.appendChild(newTD1);
        newTR.appendChild(newTD2);
        newTR.appendChild(newTD3);
        newTR.appendChild(newTD4);
        newTR.appendChild(newTD5);
        newTR.appendChild(newTD6);
        newTR.appendChild(newTD7);
        newTR.appendChild(newTD8);
        //newTR.appendChild(newTD9);

        let newSelection = document.querySelector("#reimbTableBody");
        newSelection.appendChild(newTR);


    }
}

function domManipulateUser(ourJSON) {
    let element = document.getElementById('userinfo');

    let text = document.createTextNode("Welcome, " + ourJSON.firstName + ".");

    element.appendChild(text);
}