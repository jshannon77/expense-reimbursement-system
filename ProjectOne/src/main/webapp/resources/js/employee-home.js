/**
 * 
 */

console.log("in employee js file");


window.onload = function () {
    getCurrentUser();
    getAllUsers();
    getUserReimbs();

    let logoutButton = document.getElementById("logout");
    logoutButton.addEventListener('click', function () {
        document.location.href = 'http://localhost:9001/ProjectOne';
    });

    let addForm = document.getElementById("addReimb");
    addForm.addEventListener('submit', addReimbursement);

}

//adding event listener to submit button in order to check inputs
let addForm = document.getElementById("addReimb");
//addForm.addEventListener('submit', checkInput);

function checkInput(eve) {
    //eve.preventDefault();
    const { amount, type, description } = eve.target;

    const myJSON = {
        amount: amount.value,
        typeId: type.value,
        description: description.value
    }

    console.log(amount);
    console.log('in check input' + eve);
    //first we have to check if the amount entered is a number
    //eve.preventDefault();
    let amountEntered = document.getElementById("amount").value;

    if (!isNaN(amountEntered)) {
        //the 'other' radio button is selected by default
        //now we can fetch('addreimbursementendpoint') to add the new reimbursement request 
        //using the data we've collected
        //addForm.setAttribute("action", "/ProjectOne/api/forwarding/addReimbursement");

    } else {
        alert("Please enter a number for the amount.");
    }

    amount.value = "";
    type.value = "";
    description.value = "";


}

function addReimbursement(eve) {
    eve.preventDefault();
    const { amount, type, description } = eve.target;

    const myJSON = {
        amount: amount.value,
        typeId: type.value,
        description: description.value
    }

    if(!isNaN(amount.value)){
        fetch('http://localhost:9001/ProjectOne/api/ajax/addReimbursement',

        {
            method: 'post',
            'headers': {
                'Content-Type': 'application/json'
            },
            'body': JSON.stringify(myJSON)

        }
    )
    alert("Submission was a success! Refresh the page to see the changes.");

    amount.value="";
    type.value="";
    description.value="";
    }else{
        alert("Please enter a number for the amount.");
    }

    location.reload();
    

    

}


async function getUserReimbs() {

    const responsePayload = await fetch('http://localhost:9001/ProjectOne/api/ajax/userReimbursements');

    let myJSON = await responsePayload.json();

    console.log(myJSON);

    ourDOMManipulation(myJSON);

    // fetch('http://localhost:9001/ProjectOne/api/ajax/userReimbursements')
    //     .then(
    //         function(response1){
    //             const convertedResponse = response1.json();
    //             return convertedResponse;
    //         }
    //     ).then(
    //         function(response2){
    //             console.log(response2);
    //             ourDOMManipulation(response2);
    //         }
    //     )
}

async function getCurrentUser() {

    const responsePayload = await fetch('http://localhost:9001/ProjectOne/api/ajax/currentUser');

    const convertedResponse = await responsePayload.json();

    console.log(convertedResponse);

    domManipuateUser(convertedResponse);


    //  fetch('http://localhost:9001/ProjectOne/api/ajax/currentUser')
    //     .then(
    //         function(userResponse1){
    //             const convertedResponse = userResponse1.json();
    //             return convertedResponse;
    //         }
    //     ).then(
    //         function(userResponse2){
    //             console.log(userResponse2);
    //             domManipuateUser(userResponse2);
    //         }
    //     )
}

let userArray = [];

async function getAllUsers() {

    const responsePayload = await fetch('http://localhost:9001/ProjectOne/api/ajax/getAllUsers');

    userArray = await responsePayload.json();

    console.log(userArray);

    // fetch('http://localhost:9001/ProjectOne/api/ajax/getAllUsers')
    //     .then(
    //         function(allUsers){
    //             const convertedResponse = allUsers.json();
    //             return convertedResponse;
    //         }
    //     ).then(
    //         function(response2){
    //             userArray = response2;
    //             console.log("user array: ");
    //             console.log(userArray);
    //         }
    //     )

}

function domManipuateUser(ourJSON) {
    let element = document.getElementById('userinfo');

    let text = document.createTextNode("Welcome, " + ourJSON.firstName + ".");

    element.appendChild(text);
}

function ourDOMManipulation(ourJSON) {
    for (let i = 0; i < ourJSON.length; i++) {

        //all creations
        let newTR = document.createElement("tr");
        let newTH = document.createElement("th");

        let newTD1 = document.createElement("td");
        let newTD2 = document.createElement("td");
        let newTD3 = document.createElement("td");
        let newTD4 = document.createElement("td");
        let newTD5 = document.createElement("td");
        let newTD6 = document.createElement("td");
        let newTD7 = document.createElement("td");
        let newTD8 = document.createElement("td");
        let newTD9 = document.createElement("td");

        //population creations
        newTH.setAttribute("scope", "row");
        let myText1 = document.createTextNode(ourJSON[i].reimbId);
        let myText2 = document.createTextNode("$"+ourJSON[i].amount);

        //switch statments for type and status
        let myText3;
        switch (ourJSON[i].typeId) {
            case 1:
                myText3 = document.createTextNode("Lodging");
                break;
            case 2:
                myText3 = document.createTextNode("Travel");
                break;
            case 3:
                myText3 = document.createTextNode("Food");
                break;
            case 4:
                myText3 = document.createTextNode("Other");
                break;
        }

        let myText4;
        switch (ourJSON[i].statusId) {
            case 1:
                myText4 = document.createTextNode("Pending");
                break;
            case 2:
                myText4 = document.createTextNode("Approved");
                break;
            case 3:
                myText4 = document.createTextNode("Denied");
                break;
        }


        //let myText3 = document.createTextNode(ourJSON[i].typeId);
        //let myText4 = document.createTextNode(ourJSON[i].statusId);
        let myText5 = document.createTextNode(ourJSON[i].submitted ? new Date(ourJSON[i].submitted).toLocaleString() : "N/A");
        let myText6 = document.createTextNode(ourJSON[i].resolved ? new Date(ourJSON[i].resolved).toLocaleString() : "N/A");
        let myText7 = document.createTextNode(ourJSON[i].description);

        //loops to determine which user is which 
        let userFullName = "";
        let resolverFullName = null;

        for (let j = 0; j < userArray.length; j++) {
            //console.log(userArray[j].userId);
            if (userArray[j].userId == ourJSON[i].authorId) {
                userFullName = userArray[j].firstName + " " + userArray[j].lastName;
            }
        }

        for (let j = 0; j < userArray.length; j++) {
            if (userArray[j].userId == ourJSON[i].resolverId) {
                resolverFullName = userArray[j].firstName + " " + userArray[j].lastName;
            }
        }

        let myText8 = document.createTextNode(userFullName);
        let myText9 = document.createTextNode(resolverFullName);


        //all appendings
        newTH.appendChild(myText1);
        newTD1.appendChild(myText2);
        newTD2.appendChild(myText3);
        newTD3.appendChild(myText4);
        newTD4.appendChild(myText5);
        newTD5.appendChild(myText6);
        newTD6.appendChild(myText7);
        newTD7.appendChild(myText8);
        newTD8.appendChild(myText9);


        newTR.appendChild(newTH);
        newTR.appendChild(newTD1);
        newTR.appendChild(newTD2);
        newTR.appendChild(newTD3);
        newTR.appendChild(newTD4);
        newTR.appendChild(newTD5);
        newTR.appendChild(newTD6);
        newTR.appendChild(newTD7);
        newTR.appendChild(newTD8);
        //newTR.appendChild(newTD9);

        let newSelection = document.querySelector("#reimbTableBody");
        newSelection.appendChild(newTR);


    }
}