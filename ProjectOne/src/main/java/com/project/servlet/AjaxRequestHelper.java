package com.project.servlet;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.project.controller.ReimbursementController;
import com.project.controller.UserController;
import com.project.model.User;

public class AjaxRequestHelper {

	public static void process(HttpServletRequest req, HttpServletResponse res) throws IOException {
		System.out.println(req.getRequestURI());
		
		switch (req.getRequestURI()) {
		case "/ProjectOne/api/ajax/allReimbursements":
			ReimbursementController.allFinder(req, res);
			break;
		case "/ProjectOne/api/ajax/currentUser":
			UserController.findCurrentUser(req, res);
			break;
		case "/ProjectOne/api/ajax/getAllUsers":
			UserController.findAllUsers(req, res);
			break;
		case "/ProjectOne/api/ajax/userReimbursements":
			ReimbursementController.someFinder(req, res);
			break;
		case "/ProjectOne/api/ajax/addReimbursement":
			ReimbursementController.addReimbursement(req, res);
			break;
		case "/ProjectOne/api/ajax/modifyReimbursement":
			ReimbursementController.modifyReimbursement(req, res);
		default:
			res.getWriter().println("null");
		}
	}
	
}
