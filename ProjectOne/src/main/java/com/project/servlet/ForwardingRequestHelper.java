package com.project.servlet;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.project.controller.HomeController;
import com.project.controller.LoginController;
import com.project.controller.ReimbursementController;

public class ForwardingRequestHelper {

	public static String process(HttpServletRequest req, HttpServletResponse res) throws JsonProcessingException, IOException {

		System.out.println("In forwarding request helper");
		System.out.println(req.getRequestURI());

		switch (req.getRequestURI()) {

		case "/ProjectOne/api/forwarding/login":
			System.out.println("case 1");
			return LoginController.login(req);

		case "/ProjectOne/api/forwarding/home":
			System.out.println("case 2");
			return HomeController.home(req, res);
		case "/ProjectOne/api/forwarding/addReimbursement":
			ReimbursementController.addReimbursement(req, res);
			return HomeController.home(req, res);
		default:
			return "/resources/html/bad-login.html";
		}
	}

}
