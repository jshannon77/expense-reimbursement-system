package com.project.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.project.model.Reimbursement;
import com.project.model.User;

public class ReimbursementDaoImpl implements ReimbursementDao {

	// variables to be used for contacting the database
	public static String url = "jdbc:postgresql://"+System.getenv("REVATURE_DB_URL")+"/projectoneDB";
	public static String username = "testbase";
	public static String password = System.getenv("REVATURE_DB_PASSWORD");

	public static String testUrl = "jdbc:h2:./folder1/theData;MODE=PostgreSQL";
	public static String testUser = "sa";
	public static String testPass = "sa";

	public ReimbursementDaoImpl() {

	}

	public ReimbursementDaoImpl(String _url, String _user, String _pass) {

		url = _url;
		username = _user;
		password = _pass;

	}

	@Override
	public List<Reimbursement> getAllReimbursementsFromDB() {

		try (Connection conn = DriverManager.getConnection(url, username, password)) {

			String sql = "SELECT * FROM ers_reimbursement;";

			PreparedStatement ps = conn.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();

			List<Reimbursement> returnList = new ArrayList<>();

			// adding values to the list to be returned
			while (rs.next()) {
				returnList.add(new Reimbursement(rs.getInt(1), rs.getDouble(2), rs.getTimestamp(3), rs.getTimestamp(4),
						rs.getString(5), rs.getBytes(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10)));
			}

			return returnList;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public List<Reimbursement> getReimbursementByUser(User user) {

		try (Connection conn = DriverManager.getConnection(url, username, password)) {

			String sql = "SELECT * FROM ers_reimbursement r WHERE r.reimb_author = ?;";

			PreparedStatement ps = conn.prepareStatement(sql);

			ps.setInt(1, user.getUserId());

			ResultSet rs = ps.executeQuery();

			List<Reimbursement> returnList = new ArrayList<>();

			// adding values to the list to be returned
			while (rs.next()) {
				returnList.add(new Reimbursement(rs.getInt(1), rs.getDouble(2), rs.getTimestamp(3), rs.getTimestamp(4),
						rs.getString(5), rs.getBytes(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10)));
			}

			return returnList;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public boolean addReimbursementToDB(Reimbursement reimb) {

		try (Connection conn = DriverManager.getConnection(url, username, password)) {

			String sql = "INSERT INTO ers_reimbursement (reimb_amount,reimb_submitted,reimb_resolved,reimb_description,reimb_receipt,reimb_author,reimb_resolver,reimb_status_id,reimb_type_id) VALUES \r\n"
					+ "(?,current_timestamp,NULL,?,null,?,null,1,?);";

			PreparedStatement ps = conn.prepareStatement(sql);

			ps.setDouble(1, reimb.getAmount());
			ps.setString(2, reimb.getDescription());
			ps.setInt(3, reimb.getAuthorId());
			ps.setInt(4, reimb.getTypeId());

			int i = ps.executeUpdate();

			if (i > 0) {
				return true;
			} else {
				return false;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

	@Override
	public void updateReimbursementInDB(Reimbursement newReimb, User u) {

		try (Connection conn = DriverManager.getConnection(url, username, password)) {

			String sql = "UPDATE ers_reimbursement SET reimb_status_id = ?, reimb_resolver = ?, reimb_resolved = current_timestamp WHERE reimb_id = ?;";

			PreparedStatement ps = conn.prepareStatement(sql);

			ps.setInt(1, newReimb.getStatusId());
			ps.setInt(2, u.getUserId());
			ps.setInt(3, newReimb.getReimbId());

			int i = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void h2InitDao() {
		try (Connection conn = DriverManager.getConnection(testUrl, testUser, testPass)) {

			// creating reimbursement table
			String reimbSql = "CREATE TABLE ers_reimbursement( reimb_id serial PRIMARY KEY " + ", reimb_amount numeric "
					+ ", reimb_submitted timestamp " + ", reimb_resolved timestamp "
					+ ", reimb_description varchar(250)" + ", reimb_receipt bytea" + ", reimb_author integer"
					+ ", reimb_resolver integer" + ", reimb_status_id integer" + ", reimb_type_id integer);"
					+ "INSERT INTO ers_reimbursement (reimb_amount,reimb_submitted,reimb_resolved,reimb_description,reimb_receipt,reimb_author,reimb_resolver,reimb_status_id,reimb_type_id) VALUES"
					+ "(250,current_timestamp,NULL,'test reimbursement',null,1,null,1,2);"
					+ "INSERT INTO ers_reimbursement (reimb_amount,reimb_submitted,reimb_resolved,reimb_description,reimb_receipt,reimb_author,reimb_resolver,reimb_status_id,reimb_type_id) VALUES \r\n"
					+ "(300,current_timestamp,NULL,'test 2 reimbursement',null,1,null,1,2);";

			Statement accState = conn.createStatement();
			accState.execute(reimbSql);

			// creating users table
			String fSql = "CREATE TABLE ers_users(" + " ers_users_id serial PRIMARY KEY"
					+ ", ers_username varchar(50) UNIQUE " + ", ers_password varchar(50)"
					+ ", user_first_name varchar(100) " + ", user_last_name varchar(100)"
					+ ", user_email varchar(150) UNIQUE " + ", user_role_id integer);"
					+ "INSERT INTO ers_users (ers_username, ers_password, user_first_name, user_last_name, user_email, user_role_id) VALUES ('jshan','jshan','Joe','Shannon','jws@mail.com',2);"
					+ "INSERT INTO ers_users (ers_username, ers_password, user_first_name, user_last_name, user_email, user_role_id) VALUES ('jshanuser','jshan','John','Shannon','jas@mail.com',1);";

			Statement fState = conn.createStatement();
			fState.execute(fSql);

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void h2DestroyDao() {
		// getting rid of our test tables
		try (Connection conn = DriverManager.getConnection(testUrl, testUser, testPass)) {

			String sql = "DROP TABLE ers_reimbursement;" + "DROP TABLE ers_users;";

			Statement state = conn.createStatement();
			state.execute(sql);

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
