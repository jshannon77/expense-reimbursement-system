package com.project.dao;

import java.util.List;

import com.project.model.Reimbursement;
import com.project.model.User;

public interface ReimbursementDao {

	public List<Reimbursement> getAllReimbursementsFromDB();

	public List<Reimbursement> getReimbursementByUser(User user);

	public boolean addReimbursementToDB(Reimbursement reimb);

	public void updateReimbursementInDB(Reimbursement newReimb, User u);

	// h2 database functionality
	public void h2InitDao();

	public void h2DestroyDao();

}
