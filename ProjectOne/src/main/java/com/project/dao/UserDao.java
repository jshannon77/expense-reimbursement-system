package com.project.dao;

import java.util.List;

import com.project.model.User;

public interface UserDao {

	public User getUserFromDB(String username);
	public List<User> getAllUsersFromDB();
}
