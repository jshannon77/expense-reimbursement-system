package com.project.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.project.model.User;

public class UserDaoImpl implements UserDao {

	// variables to be used for contacting the database
	public static String url = "jdbc:postgresql://"+System.getenv("REVATURE_DB_URL")+"/projectoneDB";
	public static String username = "testbase";
	public static String password = System.getenv("REVATURE_DB_PASSWORD");
	
	public static String testUrl = "jdbc:h2:./folder1/theData;MODE=PostgreSQL";
	public static String testUser = "sa";
	public static String testPass = "sa";

	public UserDaoImpl() {

	}

	public UserDaoImpl(String _url, String _user, String _pass) {

		url = _url;
		username = _user;
		password = _pass;

	}

	@Override
	public User getUserFromDB(String user) {

		try (Connection conn = DriverManager.getConnection(url, username, password)) {
			
			String sql = "SELECT * FROM ers_users u WHERE u.ers_username = ?;";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ps.setString(1, user);
			
			ResultSet rs = ps.executeQuery();
			
			User uToReturn = null;
			
			while(rs.next()) {
				uToReturn = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4),
						rs.getString(5), rs.getString(6), rs.getInt(7));
			}
			
			//constructs user object from result set and returns it
			return uToReturn;
			
			
		}catch(SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public List<User> getAllUsersFromDB() {

		try(Connection conn = DriverManager.getConnection(url, username, password)){
			
			String sql = "SELECT * FROM ers_users;";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ResultSet rs = ps.executeQuery();
			
			List<User> users = new ArrayList<>();
			
			while(rs.next()) {
				users.add(new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4),
					rs.getString(5), rs.getString(6), rs.getInt(7)));
			}
			
			return users;
					
		}catch(SQLException e) {
			e.printStackTrace();
		}
		
		return null;
	}

}
