package com.project.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.project.model.Reimbursement;
import com.project.model.User;
import com.project.service.ReimbursementService;
import com.project.service.ReimbursementServiceImpl;
import com.project.service.UserService;
import com.project.service.UserServiceImpl;

public class LoginController {
	
	final static Logger loggy = Logger.getLogger(LoginController.class);

	public static String login(HttpServletRequest req) {
		
		// making sure that we are being accessed via a POST method
		// takes user back to main page if not
		if (!req.getMethod().equals("POST")) {
			return "/index.html";
		}

		// now we extract data from the form
		String username = req.getParameter("username");
		String password = req.getParameter("password");

		// now we check to see that the user has the proper credentials
		// calling verify credentials method from the service layer
		UserService useServ = new UserServiceImpl();
		ReimbursementService reimbServ = new ReimbursementServiceImpl();

		if (useServ.verifyLogin(username, password)) {
			// if condition is met, we will add the user object to the session
			// and forward user to the home page corresponding to their account type

			User userToPass = useServ.getUser(username);
			List<Reimbursement> userReimbs = reimbServ.getReimbursementsByUser(userToPass);
			
			System.out.println(userToPass);
			req.getSession().setAttribute("loggedUser", userToPass);
			req.getSession().setAttribute("userReimbs", userReimbs);

			return "/api/forwarding/home";

		} else {
			loggy.info("User has input incorrect credentials and is being sent to the bad login page.");
			return "/resources/html/bad-login.html";
		}
	}

}
