package com.project.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.project.model.Reimbursement;
import com.project.model.User;
import com.project.service.ReimbursementService;
import com.project.service.ReimbursementServiceImpl;
import com.project.servlet.AjaxRequestHelper;

public class ReimbursementController {
	
	final static Logger loggy = Logger.getLogger(ReimbursementController.class);

	static ReimbursementService reimbServ = new ReimbursementServiceImpl();
	
	public static void allFinder(HttpServletRequest req, HttpServletResponse res) throws JsonProcessingException, IOException {

		User u = (User) req.getSession().getAttribute("loggedUser");
		
		loggy.info(u.getFirstName()+" "+u.getLastName()+" is requesting all reimbursements.");
		
		List<Reimbursement> allReimbs = reimbServ.getAllReimbursements(); 
		
		System.out.println(allReimbs);
		
		res.getWriter().write(new ObjectMapper().writeValueAsString(allReimbs));
		
	}
	
	public static void someFinder(HttpServletRequest req, HttpServletResponse res) throws JsonProcessingException, IOException {
		
		User u = (User) req.getSession().getAttribute("loggedUser");
		
		loggy.info(u.getFirstName()+" "+u.getLastName()+" is requesting their reimbursements.");
		
		List<Reimbursement> userReimbs = reimbServ.getReimbursementsByUser(u);
		
		res.getWriter().write(new ObjectMapper().writeValueAsString(userReimbs));
		
		
		
	}

	/**
	 * This method is used to get form data from the req parameter and add
	 * a new reimbursement to the DB with the given properties
	 * 
	 * @param req
	 * @param res
	 * @return 
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonParseException 
	 */
	public static void addReimbursement(HttpServletRequest req, HttpServletResponse res) throws JsonParseException, JsonMappingException, IOException {
		
		ObjectMapper mapper = new ObjectMapper();
		
		Reimbursement newReimb = mapper.readValue(req.getInputStream(), Reimbursement.class);
		
		
		User u = (User) req.getSession().getAttribute("loggedUser");

//		Double amount = Double.parseDouble(req.getParameter("amount"));
//		
//		String description = req.getParameter("description");
//		
//		int type = Integer.parseInt(req.getParameter("type"));
		
		//constructing reimbursement object with the given information
		//Reimbursement newReimb = new Reimbursement();
		//newReimb.setAmount(amount);
		//newReimb.setDescription(description);
		newReimb.setAuthorId(u.getUserId());
		//newReimb.setTypeId(type);
		
		
		//call to service layer should go here
		reimbServ.addReimbursement(newReimb);
		
	}

	/**
	 * 
	 * @param req
	 * @param res
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	public static void modifyReimbursement(HttpServletRequest req, HttpServletResponse res) throws JsonParseException, JsonMappingException, IOException {

		ObjectMapper mapper = new ObjectMapper();
		
		User u = (User) req.getSession().getAttribute("loggedUser");
		
		Reimbursement newReimb = mapper.readValue(req.getInputStream(), Reimbursement.class);
		
		reimbServ.updateReimbursement(newReimb, u);
		
	}
	
	

	

}
