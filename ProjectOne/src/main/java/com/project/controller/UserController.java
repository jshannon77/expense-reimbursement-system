package com.project.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.project.model.User;
import com.project.service.UserService;
import com.project.service.UserServiceImpl;

public class UserController {
	
	final static Logger loggy = Logger.getLogger(UserController.class);

	public static void findCurrentUser(HttpServletRequest req, HttpServletResponse res) throws JsonProcessingException, IOException {

		User u = (User) req.getSession().getAttribute("loggedUser");
		
		loggy.info("Current user info is being requested by the client");
		
		System.out.println(u);
		
		res.getWriter().write(new ObjectMapper().writeValueAsString(u));
		
	}

	public static void findAllUsers(HttpServletRequest req, HttpServletResponse res) throws JsonProcessingException, IOException {

		UserService useServ = new UserServiceImpl();
		
		loggy.info("All user info is being requested by the client");
		
		List<User> users = useServ.getAllUsers();
		
		System.out.println(users);
		
		res.getWriter().write(new ObjectMapper().writeValueAsString(users));
		
	}
	
}
