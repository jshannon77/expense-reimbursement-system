package com.project.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.project.model.User;

public class HomeController {
	
	final static Logger loggy = Logger.getLogger(HomeController.class);

	public static String home(HttpServletRequest req, HttpServletResponse res) throws JsonProcessingException, IOException {
		
		User u = (User) req.getSession().getAttribute("loggedUser");
		
		//if finance manager, send them to the manager home page
		if(u.getRoleId()==2) {
			loggy.info(u.getFirstName()+" "+u.getLastName()+" is being sent to the manager home page.");
			return "/resources/html/manager-home.html";
		}else {
			loggy.info(u.getFirstName()+" "+u.getLastName()+" is being sent to the employee home page.");
			return "/resources/html/employee-home.html";
		}
		
		
	}

}
