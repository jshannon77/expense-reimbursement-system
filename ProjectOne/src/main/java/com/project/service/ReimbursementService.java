package com.project.service;

import java.util.List;

import com.project.model.Reimbursement;
import com.project.model.User;

public interface ReimbursementService {

	public List<Reimbursement> getAllReimbursements();
	public List<Reimbursement> getReimbursementsByUser(User user);
	public void addReimbursement(Reimbursement r);
	public void updateReimbursement(Reimbursement newReimb, User u);
}
