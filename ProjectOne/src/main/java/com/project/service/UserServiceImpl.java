package com.project.service;

import java.util.List;

import com.project.dao.UserDao;
import com.project.dao.UserDaoImpl;
import com.project.model.User;

public class UserServiceImpl implements UserService {

	private UserDao useDao = new UserDaoImpl();
	
	@Override
	public boolean verifyLogin(String user, String pass) {
		
		//getting user from the DB
		User u = useDao.getUserFromDB(user);
		
		//checking if account attached to that username
		//if it does, check if password matches
		//if so, return true
		//all else, return false
		if (u == null) {
			return false;
		}else {
			if(u.getPassword().equals(pass)) {
				return true;
			}else {
				return false;
			}
		}
	}

	@Override
	public User getUser(String user) {

		User u = useDao.getUserFromDB(user);
		
		return u;
	}

	@Override
	public List<User> getAllUsers() {

		List<User> users = useDao.getAllUsersFromDB();
		
		return users;
		
	}

}
