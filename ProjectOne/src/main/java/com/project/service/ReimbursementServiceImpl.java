package com.project.service;

import java.util.List;

import com.project.dao.ReimbursementDao;
import com.project.dao.ReimbursementDaoImpl;
import com.project.model.Reimbursement;
import com.project.model.User;

public class ReimbursementServiceImpl implements ReimbursementService {
	
	ReimbursementDao reimbDao = new ReimbursementDaoImpl();

	@Override
	public List<Reimbursement> getAllReimbursements() {
		
		List<Reimbursement> reimbsToReturn = reimbDao.getAllReimbursementsFromDB();
		
		return reimbsToReturn;
	}

	@Override
	public List<Reimbursement> getReimbursementsByUser(User user) {

		List<Reimbursement> reimbsToReturn = reimbDao.getReimbursementByUser(user);
		
		return reimbsToReturn;
	}

	@Override
	public void addReimbursement(Reimbursement r) {

		reimbDao.addReimbursementToDB(r);
		
	}

	@Override
	public void updateReimbursement(Reimbursement newReimb, User u) {

		reimbDao.updateReimbursementInDB(newReimb, u);
		
	}

}
