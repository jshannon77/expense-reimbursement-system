package com.project.service;

import java.util.List;

import com.project.model.User;

public interface UserService {
	public boolean verifyLogin(String user, String pass);
	public User getUser(String user);
	public List<User> getAllUsers();
}
