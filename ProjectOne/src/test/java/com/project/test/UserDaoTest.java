package com.project.test;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.project.dao.ReimbursementDao;
import com.project.dao.ReimbursementDaoImpl;
import com.project.dao.UserDao;
import com.project.dao.UserDaoImpl;
import com.project.model.User;

public class UserDaoTest {
	
	static ReimbursementDao reimbDao;
	static UserDao useDao;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
		reimbDao = new ReimbursementDaoImpl("jdbc:h2:./folder1/theData;MODE=PostgreSQL","sa", "sa");
		useDao = new UserDaoImpl("jdbc:h2:./folder1/theData;MODE=PostgreSQL","sa", "sa");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		reimbDao.h2InitDao();
	}

	@After
	public void tearDown() throws Exception {
		reimbDao.h2DestroyDao();
	}

	@Test
	public void getAllUsersTest() {
		List<User> uList = useDao.getAllUsersFromDB();
		
		assertEquals(1, uList.get(0).getUserId());
		assertEquals(2, uList.get(1).getUserId());
		assertEquals("jshan", uList.get(0).getUsername());
		assertEquals("jshanuser", uList.get(1).getUsername());
		assertEquals("jshan", uList.get(0).getPassword());
		
	}
	
	@Test
	public void getSingleUserTest() {
		
		User uGotten = useDao.getUserFromDB("jshan");
		
		assertEquals("jshan", uGotten.getUsername());
		assertEquals("jshan", uGotten.getPassword());
		assertEquals(1, uGotten.getUserId());
		assertEquals("jws@mail.com", uGotten.getEmail());
		assertEquals(2, uGotten.getRoleId());
		
	}

}
