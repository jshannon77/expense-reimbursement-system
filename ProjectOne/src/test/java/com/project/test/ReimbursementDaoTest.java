package com.project.test;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.project.dao.ReimbursementDao;
import com.project.dao.ReimbursementDaoImpl;
import com.project.model.Reimbursement;
import com.project.model.User;

public class ReimbursementDaoTest {

	static ReimbursementDao reimbDao;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		reimbDao = new ReimbursementDaoImpl("jdbc:h2:./folder1/theData;MODE=PostgreSQL","sa", "sa");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		reimbDao.h2InitDao();
	}

	@After
	public void tearDown() throws Exception {
		reimbDao.h2DestroyDao();
	}

	@Test
	public void getAllReimbsTest() {
		
		List<Reimbursement> allReimbs = reimbDao.getAllReimbursementsFromDB();
		
		assertEquals(1, allReimbs.get(0).getReimbId());
		//assertEquals(250, allReimbs.get(0).getAmount());
		assertEquals(2, allReimbs.get(1).getReimbId());
		//assertEquals(300, allReimbs.get(1).getAmount());
		assertEquals("test reimbursement", allReimbs.get(0).getDescription());
		assertEquals("test 2 reimbursement", allReimbs.get(1).getDescription());
		assertEquals(null, allReimbs.get(0).getReceipt());
		assertEquals(null, allReimbs.get(1).getReceipt());
		
		
	}
	
	@Test
	public void getReimbByUserTest() {
		
		User u = new User();
		u.setUserId(1);
		
		List<Reimbursement> userReimbs = reimbDao.getReimbursementByUser(u);
		
		assertEquals(u.getUserId(), userReimbs.get(0).getAuthorId());
		assertEquals(u.getUserId(), userReimbs.get(1).getAuthorId());
		
		
	}
	
	@Test
	public void addReimbTest() {
		
		Reimbursement r = new Reimbursement();
		
		reimbDao.addReimbursementToDB(r);
		
		List<Reimbursement> rGotten = reimbDao.getAllReimbursementsFromDB();
		
		assertEquals(3, rGotten.get(2).getReimbId());
		
	}

}
