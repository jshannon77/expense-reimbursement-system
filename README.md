# Expense Reimbursement System
This is my submission for the Expense Reimbursement System project assigned by Revature as "Project 1".

This app allows users to log in, add new requests for reimbursements, and see all of their previous reimbursements.

Finance managers can log in, view all reimbursements in the system, approve or deny pending requests, and filter the requests by their status.

# Technologies Used
 - Maven
 - JDBC
 - Tomcat
 - BootStrap
 - Servlets
 - AJAX
 - Vanilla JS

 # Features
 - Users can log in and see their past reimbursements
 - Users can submit a new reimbursement requests
 
 - Finance managers can view ALL reimbursements in the system
 - Finance managers can filter reimbursements by their status
 - Finance managers can approve/deny reimbursements at their discretion (can only be done once)

 To do list:
 - Project works best when run in Firefox due to issues with the CORS filter...still haven't quite figured out how to fix that
 - Upon form submission, page tries to refresh and resubmit form data. Figured out how to make it so the variables are null after first submission,
 but still working on preventing that behavior.

 # Getting Started
 - Open a git bash terminal and navigate to the folder where you'd like to install this project.
 - type "git clone <url>" where <url> is the project repo URL.
 - Once you've loaded the repo, open STS in your preferred workspace and import this project from the repo into that workspace.
 
 # Usage
 - You'll first need to go into the com.project.dao package and insert your specific database credentials in the requiste dao implementation classes. As it stands, the project utilizes environment variables on my current machine, so you'll have to reconfigure those in order to make it work on your machine.
 - Once you've done this, I would suggest making a quick dummy database using an AWS RDS instance.  Be sure to follow the naming schema from my model layer precisely. I.e, reimbursements table, users table, etc. 
 - If you'd rather not do this, you could configure the included H2 dummy database to house the relevant info. You'd do this by running one of the JUnit test cases (comment out the implementation of tearDownAfterClass()) This would be a bit more complex to implement and if you have easy access to an AWS RDS instance, I'd recommend NOT doing it this way.  The choice is yours.
 - Upon doing this, I'd make a quick main method in order to test out your dummy database using the CRUD methods defined in the DaoImpl classes.
 - Make sure that you've added Tomcat to the build path of the project, and also add this project to your active Tomcat applications by right clicking the server -> add/remove -> add the project -> finish. 
 - Take note of the port that Tomcat is utilizing and start up Tomcat. 
 - Use Postman to test out the endpoints if you wish.
 - Head over to your web browser of choice (for best performance, use Firefox) and navigate to "localhost:<port>/Project2" where <port> is the port on which you've deployed the application (i.e., the port that Tomcat is making use of).
 - If you've done all of the setup properly, you should see the login page here.
 - Go ahead and log in, play around, and test out the functionality. 
